# Docky Tutorial

## Contenido

1. #### ¿Qué es Docker?
    - #### ¿Qué es un contenedor?
    - #### ¿Qué es una imagen?
2. #### ¿Porqué docker se lo usa en desarrollo?
3. #### [Instalación](contenidos/instalacion.md)
4. #### [Docker Hub](contenidos/comandos.md) 
5. #### [Dockerfile](contenidos/docker-file.md)
6. #### [Docker Compose](contenidos/docker-compose.md)
7. #### Anexos
    - #### [Configuraciones super usuario](anexos/install-sudo.md)
    - #### [Configuraciones usuario docker](anexos/conf-user-docker.md)
    - #### [Bibliografía](anexos/biblio.md)


# ![Presentacion 1](img/1.png)
# ![Presentacion 2](img/2.png)

