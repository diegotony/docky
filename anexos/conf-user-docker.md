# Configuraciones Post-Instalación 

### Agregar el grupo de docker
```
sudo groupadd docker
```
### Agregamos nuestro usuario a el grupo de docker que creamos recientemente

```
sudo gpasswd -a "nuestro_usuario" docker
```

### Activamos los cambios

```
newgrp docker
```

### Corremos un contenedor para probar si la configuracion surgio efecto

```
docker run hello-world
```
##### [Retornar a Inicio](../README.md)