# Configuraciones basicas

### Sudo
En caso de no tener el paquete sudo


# para acceder al superusuario
```
su -
```
# Actualizar paquetes
```
apt update
apt upgrade
```
# Instalar el paquete sudo
```
apt install sudo
```
# Añadir username, reemplazas USERNAME por el nombre de 
# Usuario de tu equipo
```
usermod -aG sudo USERNAME
```
##### [Retornar a Inicio](../README.md)