# Comandos Básicos de Docker
## DockerHub

## **docker pull**

1. Revisar el siguiente enlace [DockerHub](https://hub.docker.com/) 

2. Buscar el contenedor de Ubuntu
3. Ejecutar el siguiente comando
```
docker pull ubuntu
```

##  **docker search**
Para buscar desde la terminal la imagen que necesitamos
```shell
docker search ubuntu
```

## **docker images**
Para visualizar las imagenes que tenemos guardadas en el repositorio    
```
docker images
```
Sale la siguiente información

- **REPOSITORY**: Este es el nombre del repositorio o imagen. 
- TAG: Esta es la etiqueta asociada con la imagen
- **IMAGE ID**: Cada imagen está asociada con una ID única. La identificación de la imagen se representa mediante un número aleatorio de 64 dígitos hexadecimales. 
- **CREATED**: Esto indica el momento en que se creó la imagen. 
- **SIZE**: Esta categoría resalta el tamaño virtual de la imagen.


## **docker run**
Para correr el contenedor que queramos
```
docker run -it --name miImagenUbuntu ubuntu 
```
**-it es para que se pueda interactuar con el contenedor**
Hace referencia a **i** de **interactivo** y **t** de **terminal**

## Instalación paquete figlet
```
apt install figlet
```
## **docker ps**
Para ver los contenedores que estan corriendo

```
sudo docker ps -a -l
```
* **-a (all)**  para ver todos los contedores activos e inactivos
* **-l (last)** para  ver el ultimo contenedor que estaba corriendo

Sale la siguiente informacion

- **CONTAINER ID**: muestra el ID del contenedor asociado con el contenedor. El ID del contenedor es un número aleatorio de 64 dígitos hexadecimales. Por defecto, el subcomando docker ps mostrará solo 12 dígitos hexadecimales. 
- **IMAGE**: Esto muestra la imagen a partir de la cual se ha creado el contenedor Docker.
- **COMMAND**: Esto le muestra el comando ejecutado durante el lanzamiento del contenedor. 
- **CREATED**: Esto le dice cuando se creó el contenedor. 
- **STATUS**: Esto le indica el estado actual del contenedor. 
- **PORTS**: Esto le dice si algún puerto ha sido asignado al contenedor
- **NAMES**: El motor Docker genera automáticamente un nombre de contenedor aleatorio concatenando un adjetivo y un sustantivo. El ID del contenedor o su nombre se pueden utilizar para realizar más acciones en el contenedor
## **docker start**
Para correr por el id del contenedor 
```
sudo docker start ID_CONTENEDOR
```

```
docker attach id-contenedor
```
## **docker version**

Verificar version de docker
```
docker version
```
# Comandos de Sistema

Para verificar si docker funciona bien.
```
sudo service docker status
```
En tal caso que tengamos problemas para reiniciar el servicio
```
sudo service docker restart
```
# Eliminar contenedores
Detiene los contenedores y Images
```
docker image prune
```
Eliminar Imagen
```
docker rmi IMAGEN-ID
```
Eliminar Contenedor
```
docker rm CONTENEDOR-ID
```
##### [Retornar a Inicio](../README.md)