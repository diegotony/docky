# Instalacion  Docker Community Edition (CE) para

## Ubuntu 18.04 / Debian 9.6

### Actualizar Repositorios

```
$ sudo apt-get update
```

Instalar paquetes para permitir apt usar un repositorio sobre HTTPS:

```
$ sudo apt-get install \
    apt-transport-https \
    ca-certificates \
    curl \
    software-properties-common
```

Agregando la llave GPG oficial de Docker:

``` 
$ curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
```

Agregar repositorio

```
$ sudo add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu bionic stable"
```

Actualizamos repositorios

```
$ sudo apt-get update
```

Instalar Docker:

```
$ sudo apt install docker-ce
```

Compruebe que se está ejecutando:

```
$ sudo systemctl status docker
```
##### [Retornar a Inicio](../README.md)
