# Docker Compose

Permite describir un conjunto de contenedores que se relacionan entre ellos.   Las aplicaciones basadas en microservicios se prestan a usar múltiples contenedores cada uno con un servicio


## Instalación

```
sudo apt install docker-compose
```

## Requisitos
### Dockerfile
```
 FROM python:3
 
 ENV PYTHONUNBUFFERED 1
 RUN mkdir /code
 WORKDIR /code
 ADD requirements.txt /code/
 RUN pip install -r requirements.txt
 ADD . /code/
```
### docker-compose.yml
```
version: '3'

services:
  pg:
    container_name: db-pg
    image: postgres
  django:
    container_name: proyecto-django
    restart: always
    build: .
    command: python3 manage.py runserver 0.0.0.0:8000
    volumes:
      - .:/code
    ports:
      - "8000:8000"
    depends_on:
      - pg
```

### requirements.txt

```
 Django == 2.0
 psycopg2

```

## Crear un Proyecto Django
```
docker-compose run django django-admin.py startproject projectname .
```

Dar permisos para modificar archivos
```
sudo chown -R $USER:$USER .
```
## Crear una app 
```
docker-compose run django python manage.py startapp app
```

## Configurar Postgres 
```

DATABASES = {
    'default': {
    'ENGINE': 'django.db.backends.postgresql_psycopg2',
    'NAME': 'postgres',
    'USER': 'postgres',
    'PASSWORD': '',
    'HOST': 'pg',
    'PORT': '5432',
    }
}
```
  
## Crear Usuario 
```bash
# Migraciones
docker-compose run django python manage.py migrate

docker-compose run django python manage.py createsuperuser
```

## Crear Usuario 
```
docker-compose run django python manage.py createsuperuser
```

## Iniciar Servicios
```
docker-compose up
```

## Finalizar Servicios
```
docker-compose down
```
##### [Retornar a Inicio](../README.md)


