# DockerFile
Contiene las instrucciones necesarias para automatizar la creación de una imagen que será utilizada posteriormente para la ejecución . contenedores  
## Docker Build
Sirve para contruir una imagen.

```
docker build -t name_image .
```
## Composicion de Dockerfile
**FROM** Se coloca la imagen que se va usar

**RUN**  Los comandos que va a correr en esa imagen

**ENV** establecer las variables de entorno

**WORKDIR** Define el entorno de trabajo

**CMD** Se establecen comandos por defecto que se ejecutaran


``` bash
# image to install
FROM ubuntu:16.04

# Install.
RUN \
  apt-get update && \
  apt-get -y upgrade && \
  apt-get  install figlet

# Set environment variables.
ENV HOME /root

# Define working directory.
WORKDIR /root

# Define default command.
CMD ["bash"]
```
##### [Retornar a Inicio](../README.md)
